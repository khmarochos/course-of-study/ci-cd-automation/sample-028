import flask
import waitress
import argparse
import os


def get_configuration() -> dict:

    configuration = {}

    configuration_mapping = {
        'port': {
            'command_line_parameter': '--port',
            'backing_environment_variable': 'PORT',
            'description': 'The port to listen on',
            'type': int,
            'required_argument': False,
            'required': True
        },
        'hello': {
            'command_line_parameter': '--hello',
            'backing_environment_variable': 'HELLO',
            'description': 'The phrase to say',
            'type': str,
            'required_argument': False,
            'required': True
        }
    }

    argument_parser = argparse.ArgumentParser(description='Yet another demo-application.')
    for parameter, settings in configuration_mapping.items():
        argument_parser.add_argument(
            settings["command_line_parameter"],
            required=settings['required_argument'],
            dest=parameter,
            type=settings['type'],
            help="{:s} (fallback variable is {:s})".format(
                settings['description'],
                settings['backing_environment_variable']
            ),
            metavar=f'<{parameter}>'
        )

    argument_parser_result = argument_parser.parse_args()

    for parameter, settings in configuration_mapping.items():
        if getattr(argument_parser_result, parameter, None) is None:
            configuration[parameter] = os.environ.get(settings['backing_environment_variable'])
        else:
            configuration[parameter] = getattr(argument_parser_result, parameter)
        if configuration[parameter] is None and settings['required']:
            raise ValueError("Missing configuration parameter: {:s}, go get some --help".format(parameter))

    return configuration


if __name__ == '__main__':

    configuration = get_configuration()

    flask_app = flask.Flask(__name__)

    @flask_app.route('/')
    def hello_world():
        return(f"{configuration['hello']}\n")

    waitress.serve(flask_app, host='0.0.0.0', port=configuration['port'])

